#!/usr/bin/env python
# Display a runtext with double-buffering.
from samplebase import SampleBase
from rgbmatrix import graphics
import time
import requests


runWOTD = True
runWAPI = True


######WORD######
#WOTD requests
wordAPI = requests.get("https://random-word-api.herokuapp.com/word")
# formatting API info with JSON
wordJS = wordAPI.json()
# setting variable to first value in the list
word = wordJS[0]

# definition API
defAPI = requests.get("https://www.dictionaryapi.com/api/v3/references/collegiate/json/%s?key=45d6d546-327f"
                              "-4129-af11-72670cde16ef" % word)
# formatting API info with JSON
definition = defAPI.json()

# setting definition
defPrint = definition[0]['shortdef'][0]
# setting pronunciation
pronun = definition[0]['hwi']['prs'][0]['mw']

    

# 
# #####WEATHER###
#WeatherAPI requests
r = requests.get('https://api.openweathermap.org/data/2.5/onecall?lat=29.8754&lon=-97.9404&exclude=hourly,minutely&appid=198d874b3c00f942eeeeeca7d79d7e1a')
json_object = r.json()

temp_k = int(json_object['current']['temp'])
temp_f = int((temp_k - 273.15) * 1.8 + 32)
state_description = json_object['current']['weather'][0]['description']
# 
# ######TIME######
# 
# #get time info
# timeAPI = requests.get("http://worldtimeapi.org/api/timezone/America/Chicago")
# #format time info into json
# timeJS = timeAPI.json()
# #get date/time
# timePrint = timeJS['datetime']
# #get just date
# date = timePrint[5:10]
# #print date
# print(date)
# #get time 24 hours
# time24 = timePrint[11:16]
# #cast just hour and cast to int
# hour= int(timePrint[11:13])
# #get just minutes
# minutes = timePrint[13:16]
# #set 12 hour
# hour12 = hour-12
# 
# if hour < 12:
#     hour12 = hour
# # local time
# time12 = str(hour12) + minutes
# #EST
# time12est = str(hour12+1) + minutes
# #PST
# time12pst = str(hour12-2) + minutes

##############TRAFFIC####################
# retrieve json file
traffic = requests.get(
    "https://api.tomtom.com/traffic/services/4/flowSegmentData/relative0/10/json?point=52.41072%2C4.84239&unit=KMPH&openLr=false&key=KHG41eDtJ6tLnTgNkhN4LfNrC4calsBL")

# formatting API info with JSON
traffic1 = traffic.json()

trafficAPI = requests.get(
    "http://www.mapquestapi.com/directions/v2/route?key=A5cWoKRnPDNEEqvG80OQREOABS7Tyou3&from=1621+E+6th+St,Austin,TX&to=601+University+Dr,San+Marcos,TX")

# formatting API info with JSON
trafficJS = trafficAPI.json()

# Getting the number of instructions from directions
sizeMan = len(trafficJS['route']['legs'][0]['maneuvers'])


##COLORS##
red = graphics.Color(255, 0, 0)
blue = graphics.Color(0, 0, 255)
green = graphics.Color(0, 255, 0)
yellow = graphics.Color(255, 255, 0)



class RunText(SampleBase):
    def __init__(self, *args, **kwargs):
        super(RunText, self).__init__( *args, **kwargs)
        #self.parser.add_argument("-t", "--text", help="The text to scroll on the RGB LED panel", default=str(word))

    
    def run(self):
        offscreen_canvas = self.matrix.CreateFrameCanvas()
        font = graphics.Font()
        font.LoadFont("../../../fonts/7x13.bdf")
        pos = offscreen_canvas.width
                
        timeout = time.time()+300
        while True:
            if time.time()>timeout:
                break
            
            ###Display time####
            timeAPI = requests.get("http://worldtimeapi.org/api/timezone/America/Chicago")
            while True:
                
                #####TIME VARIBALES####
                #format time info into json
                timeJS = timeAPI.json()
                #get date/time
                timePrint = timeJS['datetime']
                #get just date
                date = timePrint[5:10]
                #get time 24 hours
                time24 = timePrint[11:16]
                #cast just hour and cast to int
                hour= int(timePrint[11:13])
                #get just minutes
                minutes = timePrint[13:16]
                #set 12 hour
                hour12 = hour-12

                if hour < 12:
                    hour12 = hour
                # local time
                time12 = str(hour12) + minutes
                #EST
                time12est = str(hour12+1) + minutes
                #PST
                time12pst = str(hour12-2) + minutes
                ######################################
                
                #####PRINT####
                offscreen_canvas.Clear()
                len = graphics.DrawText(offscreen_canvas, font, pos, 17, red, "Local: "+ time12 +" NYC: "+ time12est + " LA: " + time12pst)
                pos -= 1
                if (pos + len < 0):
                    pos = offscreen_canvas.width

                time.sleep(0.025)
        
                if pos==offscreen_canvas.width:
                    break
                offscreen_canvas = self.matrix.SwapOnVSync(offscreen_canvas)
            ###Display word####
            while True:
                offscreen_canvas.Clear()
                len = graphics.DrawText(offscreen_canvas, font, pos, 17, yellow, word + "  " + pronun + "  " +defPrint + " ")
                pos -= 1
                if (pos + len < 0):
                    pos = offscreen_canvas.width

                time.sleep(0.025)
                
                if pos==offscreen_canvas.width:
                    break
                offscreen_canvas = self.matrix.SwapOnVSync(offscreen_canvas)
            
            #####TRAFFIC#####
            while True:
                offscreen_canvas.Clear()
                len = graphics.DrawText(offscreen_canvas, font, pos, 17, yellow, "Current Speed: " + str(traffic1['flowSegmentData']['currentSpeed']) + " Is a road closed? " + str(traffic1['flowSegmentData']['roadClosure']))
                pos -= 1
                if (pos + len < 0):
                    pos = offscreen_canvas.width

                time.sleep(0.025)
                
                if pos==offscreen_canvas.width:
                    break
                offscreen_canvas = self.matrix.SwapOnVSync(offscreen_canvas)
            
            ###Display weather###
            while True:
                ##PRINT##
                state_description = json_object['current']['weather'][0]['description']
                offscreen_canvas.Clear()
                len = graphics.DrawText(offscreen_canvas, font, pos, 17, blue, str(temp_f) + "F  " + state_description)
                pos -= 1
                if (pos + len < 0):
                    pos = offscreen_canvas.width

                time.sleep(0.025)
                
                if pos==offscreen_canvas.width:
                    break
                offscreen_canvas = self.matrix.SwapOnVSync(offscreen_canvas)
            
            
            

# Main function
if __name__ == "__main__":
    run_text = RunText()
    if (not run_text.process()):
        run_text.print_help()

