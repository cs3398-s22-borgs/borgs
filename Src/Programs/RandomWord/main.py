import requests

def wotd():
    # random word API
    wordAPI = requests.get("https://random-word-api.herokuapp.com/word")
    # formatting API info with JSON
    wordJS = wordAPI.json()
    # setting variable to first value in the list
    word = wordJS[0]

    # definition API
    defAPI = requests.get("https://www.dictionaryapi.com/api/v3/references/collegiate/json/%s?key=45d6d546-327f"
                          "-4129-af11-72670cde16ef" % word)
    # formatting API info with JSON
    definition = defAPI.json()

    # try except to handle error
    try:
        # setting definition
        defPrint = definition[0]['shortdef'][0]
        # setting pronunciation
        pronun = definition[0]['hwi']['prs'][0]['mw']
        # printing word and capitalizing first letter
        print(word.capitalize())
        # printing pronunciation
        print(pronun.capitalize())
        # printing definition and capitalizing first letter
        print(defPrint.capitalize())
    # if there is an error (word isn't in dictionary) run program again
    except:
        wotd()


if __name__ == "__main__":
    wotd()