from astral import LocationInfo
import datetime
from astral.sun import sun

loc = LocationInfo(timezone='US/Central')
print(loc)

s = sun(loc.observer, date=datetime.date.today(), tzinfo=loc.timezone)
for key in ['dawn', 'dusk', 'noon', 'sunrise', 'sunset']:
    print(f'{key:10s}:', s[key])
