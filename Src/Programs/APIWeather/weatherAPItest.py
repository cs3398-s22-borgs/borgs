import requests

#setup API with openweathermap and access the API Info with API key
r = requests.get('https://api.openweathermap.org/data/2.5/onecall?lat=29.8754&lon=-97.9404&exclude=hourly,minutely&appid=198d874b3c00f942eeeeeca7d79d7e1a')

json_object = r.json()
#print(json_object['current'])

#get temp in k and get it from json object called"main" then from sub-object called"temp"
temp_k = int(json_object['current']['temp'])
#convert temp k to F
temp_f = int((temp_k - 273.15) * 1.8 + 32)
print(str(temp_f) +'°')

#get feels like temp and convert to F degreed
feel_temp = int(json_object['current']['feels_like'])
feel_tempf = int((feel_temp - 273.15) * 1.8 + 32)

#if feels like temp is close to normal temperature by 5 degreeds then it will not display feels like temp
if feel_tempf >= (temp_f+5) or feel_tempf <= (temp_f-5):
    print('Feels like: '+ str(feel_tempf) +'°')

#get the weather state"s description
state_description = json_object['current']['weather'][0]['description']
print(state_description.capitalize())

###############################################################################################################

uv = int(json_object['current']['uvi'])

if uv > 0:
    print('UV Index: ' + str(uv))


