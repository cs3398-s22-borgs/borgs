import datetime
import pytz

eastern = pytz.timezone('US/Eastern')
sa_time = datetime.datetime.now(eastern)

LOCAL_TIMEZONE = datetime.datetime.now(datetime.timezone.utc).astimezone().tzinfo
start_time = datetime.datetime.now()

while True:
    if (datetime.datetime.now() - start_time).seconds == 1:
        start_time = datetime.datetime.now()
        print(start_time, LOCAL_TIMEZONE)       #Print local time and time zone
        print(sa_time, eastern)                 #Print eastern time and time zone

