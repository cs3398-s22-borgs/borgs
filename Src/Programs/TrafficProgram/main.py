import requests
class Traffic():
    
    def traffic1(self):
        # retrieve json file
        traffic = requests.get(
            "https://api.tomtom.com/traffic/services/4/flowSegmentData/relative0/10/json?point=52.41072%2C4.84239&unit=KMPH&openLr=false&key=KHG41eDtJ6tLnTgNkhN4LfNrC4calsBL")

        # formatting API info with JSON
        traffic1 = traffic.json()

        trafficAPI = requests.get(
            "http://www.mapquestapi.com/directions/v2/route?key=A5cWoKRnPDNEEqvG80OQREOABS7Tyou3&from=1621+E+6th+St,Austin,TX&to=601+University+Dr,San+Marcos,TX")

        # formatting API info with JSON
        trafficJS = trafficAPI.json()

        # Getting the number of instructions from directions
        sizeMan = len(trafficJS['route']['legs'][0]['maneuvers'])

        # route = trafficJS['route']['legs'][0]['maneuvers'][0]['narrative']

        try:
            # looping thru all the directions
            for x in range(0, sizeMan):
                print(trafficJS['route']['legs'][0]['maneuvers'][x]['narrative'])

            print("Current Traffic Speed in MPH")
            print(traffic1['flowSegmentData']['currentSpeed'])

            print("Any road Closure?")
            print(traffic1['flowSegmentData']['roadClosure'])
        except:
            self.traffic1()


if __name__ == "__main__":
    traffic = traffic()
    traffic.traffic1()