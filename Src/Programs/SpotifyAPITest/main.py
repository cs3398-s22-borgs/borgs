import requests

spotify_create_playlist = 'https://api.spotify.com/v1/users/nick2115/playlists'

access_token = 'BQCItWuEjg9LhfpM1fUGbk4s2S8tT21hS-V1FjhfgfNtFNj2z4kBX5mC34a4D_W0uusw9LrLd4cVEzBopu_SsUhxXP3tAgiJe9YjQfsRunoAhzj-iUdo0_i5S8afmC64lmX8ApsXTo0m2RfIwrR34PTPQfx5A7_C7oDL6iOm34Z3DXRhRTQHGhl6W2D6JVD9Lc8PkMX8ng'

def createPlaylist(name, public):
    #request out to spotify playlist api
    respone = requests.post(
        spotify_create_playlist,
        #passing authorization token
        headers={
            "Authorization": f"Bearer {access_token}"
        },
        json={
            "name": name,
            "public": public
        }
    )
    json_resp = respone.json()

    return json_resp

def main():
    playlist=createPlaylist(
        #playlist name
        name="SpotifyAPI Test Private",
        #private playlist
        public=False
    )

    print(f"Playlist: {playlist}")

if __name__ == '__main__':
    main()