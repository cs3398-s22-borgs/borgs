##TIME##
while True:
            if time.time()>timeout:
                break
           
            ###Display time####
            timeAPI = requests.get("http://worldtimeapi.org/api/timezone/America/Chicago")
            while True:
               
                #####TIME VARIBALES####
                #format time info into json
                timeJS = timeAPI.json()
                #get date/time
                timePrint = timeJS['datetime']
                #get just date
                date = timePrint[5:10]
                #get time 24 hours
                time24 = timePrint[11:16]
                #cast just hour and cast to int
                hour= int(timePrint[11:13])
                #get just minutes
                minutes = timePrint[13:16]
                #set 12 hour
                hour12 = hour-12

                if hour < 12:
                    hour12 = hour
                # local time
                time12 = str(hour12) + minutes
                #EST
                time12est = str(hour12+1) + minutes
                #PST
                time12pst = str(hour12-2) + minutes
                ######################################
               
                #####PRINT####
                offscreen_canvas.Clear()
                len = graphics.DrawText(offscreen_canvas, font, pos, 17, red, "Local: "+ time12 +" NYC: "+ time12est + " LA: " + time12pst)
                pos -= 1
                if (pos + len < 0):
                    pos = offscreen_canvas.width

                time.sleep(0.025)
       
                if pos==offscreen_canvas.width:
                    break
                offscreen_canvas = self.matrix.SwapOnVSync(offscreen_canvas)