#!/usr/bin/env python
from samplebase import SampleBase
from rgbmatrix import graphics
import time
#import datetime
#import pytz
import requests


class GraphicsTest(SampleBase):
    def __init__(self, *args, **kwargs):
        super(GraphicsTest, self).__init__(*args, **kwargs)

    def run(self):
        canvas = self.matrix
        font = graphics.Font()
        font.LoadFont("../../../fonts/5x8.bdf")
        red = graphics.Color(255, 0, 0)
        

        green = graphics.Color(0, 255, 0)
        yellow = graphics.Color(255, 255, 0)
        
        #graphics.DrawText(canvas, font, 2, 25, yellow, str(state_description.capitalize()))

        blue = graphics.Color(0, 0, 255)
        #graphics.DrawText(canvas, font, 2, 10, yellow, str(temp_f)+"*")
        
        
        # random word API
        wordAPI = requests.get("https://random-word-api.herokuapp.com/word?number=1&swear=0")
        # formatting API info with JSON
        wordJS = wordAPI.json()
        # setting variable to first value in the list
        word = wordJS[0]

        # definition API
        defAPI = requests.get("https://www.dictionaryapi.com/api/v3/references/collegiate/json/%s?key=45d6d546-327f"
                              "-4129-af11-72670cde16ef" % word)
        # formatting API info with JSON
        definition = defAPI.json()

        # try except to handle error
        try:
            # setting definition
            defPrint = definition[0]['shortdef'][0]
            # setting pronunciation
            pronun = definition[0]['hwi']['prs'][0]['mw']
            graphics.DrawText(canvas, font, 2, 9, yellow, word.capitalize())
            graphics.DrawText(canvas, font, 2, 18, yellow, pronun.capitalize())
            graphics.DrawText(canvas, font, 2, 27, yellow, defPrint.capitalize())

            # printing word and capitalizing first letter
            print(word.capitalize())
            # printing pronunciation
            print(pronun.capitalize())
            # printing definition and capitalizing first letter
            print(defPrint.capitalize())
        # if there is an error (word isn't in dictionary) run program again
        except:
            run()

        

        time.sleep(30)   # show display for 10 seconds before exit


# Main function
if __name__ == "__main__":
    graphics_test = GraphicsTest()
    if (not graphics_test.process()):
        graphics_test.print_help()



