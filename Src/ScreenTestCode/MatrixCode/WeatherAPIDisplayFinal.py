#!/usr/bin/env python
from samplebase import SampleBase
from rgbmatrix import graphics
import time
# import datetime
# import pytz
import requests


class GraphicsTest(SampleBase):
    def __init__(self, *args, **kwargs):
        super(GraphicsTest, self).__init__(*args, **kwargs)

    def run(self):
        canvas = self.matrix
        font = graphics.Font()
        font.LoadFont("../../../fonts/7x13.bdf")

        r = requests.get(
            'https://api.openweathermap.org/data/2.5/onecall?lat=29.8754&lon=-97.9404&exclude=hourly,minutely&appid=198d874b3c00f942eeeeeca7d79d7e1a')
        wordAPI = requests.get("https://random-word-api.herokuapp.com/word?number=1&swear=0")
        # formatting API info with JSON
        json_object = r.json()
        wordJS = wordAPI.json()
        # setting variable to first value in the list
        word = wordJS[0]

        temp_k = int(json_object['current']['temp'])
        temp_f = int((temp_k - 273.15) * 1.8 + 32)
        state_description = json_object['current']['weather'][0]['description']

        red = graphics.Color(255, 0, 0)
        # graphics.DrawLine(canvas, 5, 5, 22, 13, red)
        # graphics.Drawtext(canvas, font, 5, 15, red, LOCAL_TIMEZONE)

        green = graphics.Color(0, 255, 0)
        yellow = graphics.Color(255, 255, 0)
        # graphics.DrawCircle(canvas, 15, 15, 10, green)
        graphics.DrawText(canvas, font, 2, 25, yellow, str(state_description.capitalize()))

        blue = graphics.Color(0, 0, 255)
        graphics.DrawText(canvas, font, 2, 10, yellow, str(temp_f) + "*")
        # graphics.DrawText(canvas, font, 5, 15, blue, )

        time.sleep(30)  # show display for 10 seconds before exit


# Main function
if __name__ == "__main__":
    graphics_test = GraphicsTest()
    if (not graphics_test.process()):
        graphics_test.print_help()