# Borg Visualizer
> Description

Team members: Nicholas Masih, Anderson Nguyen, Uyen Nguyen , Ringtarih Tamfu

Creating a screen visualizer

For spotify users and people interested in a lifestyle hub that displays useful information to the user.
To display daily information and cover art in a hub to the user in a pleasing and concise way


## Table of Contents
* [General Info](#general-information)
* [Technologies Used](#technologies-used)
* [Features](#features)
* [Screenshots](#screenshots)
* [Contributions](#contributions)
* [Usage](#usage)
* [Project Status](#project-status)
* [Next Steps](#next-steps)
* [Acknowledgements](#acknowledgements)
* [Contact](#contact)
<!-- * [License](#license) -->


## General Information
- Cost should be under $90

## Technologies Used
- Raspberry Pi 4  
- 32x32 LED Light Matrix
- Microphone for raspberry pi
- Spotify API
- Weather and Traffic API


## Features
List the ready features here:

- Time: 
Display the current time to the visualizer. Display time could help everyone see the time, display time is needed for everyone in daily life. (Last story in assignment 6)
- Weather: 
Display the weather to the visualizer. The display weather will help everyone to see weather during the day, and help people predict what is the weather outside when they leave the house. This display weather will be needed by everyone (2nd story in assignment 6)
- Word of the Day:
Display a word, it's definition, and pronunciation
- POSSIBLE OPTIONS: 
Display calendar date


## Screenshots
![Example screenshot](https://i.ytimg.com/vi/6i8kzqvh94E/mqdefault.jpg)
<!-- If you have screenshots you'd like to share, include them here. -->

## Contributions
Nick:
I first researched APIs and watched some YouTube videos to familiarize myself with how APIs worked and how to use them in python.

gitkraken://repolink/0abc55245efec5ccdfa9509557948d586d87394d/commit/64ac5ba051dc7c99bc5a46229735da3c1e2a2197?url=https%3A%2F%2Fnim21%40bitbucket.org%2Fcs3398-s22-borgs%2Fborgs.git

I created the word of the day program. This prgoram pulls from two APIs. One to get the random word then I pass that random word to another API to get the pronunciation and the definition.

gitkraken://repolink/0abc55245efec5ccdfa9509557948d586d87394d/commit/8612d47b4467bf996395daacf3150f7627a58e4b?url=https%3A%2F%2Fnim21%40bitbucket.org%2Fcs3398-s22-borgs%2Fborgs.git

Sprint 2:
Adjusted word of the day program to display on the led matrix:
https://bitbucket.org/cs3398-s22-borgs/borgs/commits/d62f75b7c2a5f25e4ed758ee54c9cd4952d5cabc

Created routing program to get route from location A to location B
https://bitbucket.org/cs3398-s22-borgs/borgs/commits/c86023f2dab24eac976ad32f2ac7d4723caf2e5a

Sprint 3:
Jira Task BV-55: Displayed word of the day program nicelyon the screen
https://cs3398s22borgs.atlassian.net/browse/BV-55?atlOrigin=eyJpIjoiOGQ0NDM0YjQ4YmU5NDI3N2EwMjI4YzE2M2IwNDNmZjYiLCJwIjoiaiJ9
Commit: https://bitbucket.org/cs3398-s22-borgs/borgs/commits/2034ee76f50c6990b10d84a4634d9ade57c36c5c

Jira Task BV-61: Refactor code for word of the day program
https://cs3398s22borgs.atlassian.net/browse/BV-61?atlOrigin=eyJpIjoiMDNiM2Y1MWQxODA2NGRhM2FkZWIxZDQ3MDFiNzcxMGIiLCJwIjoiaiJ9
Commit: https://bitbucket.org/cs3398-s22-borgs/borgs/commits/90a27274baa5c70c63985624e283670a675e449b

Jira Task BV-64: Create cover art display program
https://cs3398s22borgs.atlassian.net/browse/BV-64?atlOrigin=eyJpIjoiN2I0NGMxNWMyNzZjNDZhYThkY2M2NDVlYWM2NTZiYTAiLCJwIjoiaiJ9
Commit: https://bitbucket.org/cs3398-s22-borgs/borgs/commits/ace329f4075797b68529dde50c30eb14e64219b3


Uyen:
Sprint 1:

Task 1 - Research on setup Raspberry Pi with Python, and API with Python: For the first task I reseached how to setup a Rashberry Pi with the extra hardware needed and how to utilize Python3 and additional libraries required on a Rashberry Pi. This reseach was to help us understand how the Raspberry Pi works with python code which will later help me and Rightarih to write the python code required to display weather information and atmospheric conditions such as UV index from location data. Additionally I investigated how to use the requests library to interface with external APIs by using a web API that serves images of foxes.

gitkraken://repolink/0abc55245efec5ccdfa9509557948d586d87394d/commit/e79d53c157b25e1444051a654381a5db0e7901eb?url=https%3A%2F%2Fuyen_nguyen1%40bitbucket.org%2Fcs3398-s22-borgs%2Fborgs.git

Task 2 - Get API setup for Weather: For the second task, I did more reseach on public APIs that provide weather information. I decided to use openweathermap to gather the required weather data and wrote a python script that utilizes the request library to obtain weather information as a dictionary from JSON.

gitkraken://repolink/0abc55245efec5ccdfa9509557948d586d87394d/commit/f8d389c9a98429170b8a1a858558eb7608a81381?url=https%3A%2F%2Fuyen_nguyen1%40bitbucket.org%2Fcs3398-s22-borgs%2Fborgs.git

Task 3 - Display current weather of an area with longtitude and latitude: For the third task, I displayed the current temperature of an area based on the longitude and latitude of a location. This utilized the openweathermap API described in Task 2.

gitkraken://repolink/0abc55245efec5ccdfa9509557948d586d87394d/commit/f8d389c9a98429170b8a1a858558eb7608a81381?url=https%3A%2F%2Fuyen_nguyen1%40bitbucket.org%2Fcs3398-s22-borgs%2Fborgs.git

Task 4 - Display weather description and feels like temperature if applicable: For the fourth task, I displayed the feels like temperature for a specific area (latitude and longitude) if the raw temperature data from task 2 varies significantly from what local humidity and wind conditions would cause the human body to feel.

gitkraken://repolink/0abc55245efec5ccdfa9509557948d586d87394d/commit/f8d389c9a98429170b8a1a858558eb7608a81381?url=https%3A%2F%2Fuyen_nguyen1%40bitbucket.org%2Fcs3398-s22-borgs%2Fborgs.git

Sprint 2:
Task 1: Reseach on how to display Weather information on screen with Raspberry Pi, Uploaded a document with reseached information below
https://bitbucket.org/cs3398-s22-borgs/borgs/commits/24ab1ed6d962bb0273823bc41c48fd2972917504

Task 2: After reaseach on how to display Weather API on screen with Raspberry Pi, task 2 is to test and configure the WeatherAPI code from sprint1 to display on screen. Fixing and modifie weather code by adding screen function as front size named "graphics", change color using.Color, and print text using .DrawText
https://bitbucket.org/cs3398-s22-borgs/borgs/commits/c9b8d168e17cbf45021dfb3da8c02ad35669613b

Task3: Get traffic API set up using weather website by pulling information from MapQuest
https://bitbucket.org/cs3398-s22-borgs/borgs/commits/9aa0aeda0b87d108c4e709a396eab19996793760

Sprint 3:
Task 1(BV-60): Refactor code time program - This task is refactored time code, pulling API time using World Time API instead of the pervious time program which take time information from computer. Using API for this program would help simplify main file code display for LED screen which all use APIs.  
https://cs3398s22borgs.atlassian.net/browse/BV-60
Commit: https://bitbucket.org/cs3398-s22-borgs/borgs/commits/0d5c0d66266bf034885f6d38eaf63471f539bd28

Task 2(BV-56): Display the time program nicely on the screen - This task is to take the time code created from task 1, and program display time on LED screen nicely by changing the word display font size fit in the screen and changing color 
https://cs3398s22borgs.atlassian.net/browse/BV-56
Commit: https://bitbucket.org/cs3398-s22-borgs/borgs/commits/9795030f10da110a1e3504749c3f588cc9849ce8

Task 3(BV-59): Refactor code weather program – This task is to take the weather program code form sprint 2 which pulling weather from weather map API and refactor this code to display nicely on the screen with suitable word size and changing word color
https://cs3398s22borgs.atlassian.net/browse/BV-59
Commit: https://bitbucket.org/cs3398-s22-borgs/borgs/commits/269636a468f5bc8f4b179b489501bae889543d4d


Anderson:
Sprint 1:
Task 1: Learn about python libary imports and create a program that gets the current time and current time zone. Update the time in real time each second on console.
gitkraken://repolink/0abc55245efec5ccdfa9509557948d586d87394d/commit/fc97df47c6a1b55f0e28c85597cb81a790557e7e?url=https%3A%2F%2Fa_n307%40bitbucket.org%2Fcs3398-s22-borgs%2Fborgs.git

Task 2: Research more libaries imports and extend the time program that gets the time in a different time zone. Update the time in reach time each second on console Using API was optional but using libary was alot easier.
gitkraken://repolink/0abc55245efec5ccdfa9509557948d586d87394d/commit/fc97df47c6a1b55f0e28c85597cb81a790557e7e?url=https%3A%2F%2Fa_n307%40bitbucket.org%2Fcs3398-s22-borgs%2Fborgs.git

Task 3: Research how to get sunset and sunrise time in current time zone and different time zones using API. Found a method to do this using libary imports instead. Provided dawn, dusk, noon, sunrise. sunset time using astral libary and LocationInfo.
gitkraken://repolink/0abc55245efec5ccdfa9509557948d586d87394d/commit/fc97df47c6a1b55f0e28c85597cb81a790557e7e?url=https%3A%2F%2Fa_n307%40bitbucket.org%2Fcs3398-s22-borgs%2Fborgs.git

Task 4: Set up a forming using flask web app. First render template in main python and then create html file in template to render a form. Form takes in input and saves its id so it can be used in scripting portion.
gitkraken://repolink/0abc55245efec5ccdfa9509557948d586d87394d/commit/092f3590e77113679fb233079e13f9aa6679f8c4?url=https%3A%2F%2Fa_n307%40bitbucket.org%2Fcs3398-s22-borgs%2Fborgs.git

Sprint 2:
Task 1: Research how to display web applications on raspberry pi. Create a minimal application to test if web broswer can be displayed on the device.
https://bitbucket.org/cs3398-s22-borgs/borgs/commits/537a30314b6808ef58c7074051b48cea3ecaa8b2

Task 2:Test and configured time program to display on rasberry pi. Hardware block: rasberry pi could not download the pytz import from python libaries. Solution: rewrite time program using API instead of imports for sprint 3.
https://bitbucket.org/cs3398-s22-borgs/borgs/commits/47e7623a37eaba93705d1de040adb412ee89a6a5

Tas 3: Find api for traffic areas in designed route. Write software that utilized said api to check the traffic in the route such as road closure and current average speed on road.
https://bitbucket.org/cs3398-s22-borgs/borgs/commits/a26618535ef88a9c0c295312f1c48b0654bafc86

Sprint 3:
Jira Task BV-63: Refactor the task program so that it displays only on console. Web browser approach in earlier program did not work on rasbery pi device.
https://bitbucket.org/cs3398-s22-borgs/borgs/commits/7576d1f4cc86794951febb6de0b0fad3409582b5

Jira Task BV-62: Refactor route and traffic into a class so that when an object of the class is created, a constructor will run the class program once.
https://bitbucket.org/cs3398-s22-borgs/borgs/commits/418277480e2ba963971e37d24c9ac8f0ff395ea2

Jira Task BV-57: Display route and traffic program concurrently on rasbery pi device without messing with other software program that is also being displayed. 
https://bitbucket.org/cs3398-s22-borgs/borgs/commits/a06181a140648364a2f6a9d979834085cbfd8347


Ringtarih:

Sprint 1:
Task 1: Bought and soldered the hardware to work together. This commit shows the receipt and a picture of the Adafruit Hat with the headers soldered connected to the Raspberry Pi 4.
gitkraken://repolink/0abc55245efec5ccdfa9509557948d586d87394d/commit/1edbf7dc496d479fc346bbb8792d515f8ac40571?url=https%3A%2F%2FRingtarih%40bitbucket.org%2Fcs3398-s22-borgs%2Fborgs.git

Task 2: Configured the Raspberry Pi 4 to pull from API and display on screen. This commit shows an image of the Raspberry Pi4 terminal running the code that will display the cober art on the LED matrix without the LED Matrix at the time. 
gitkraken://repolink/0abc55245efec5ccdfa9509557948d586d87394d/commit/3be90cf36aef09607b8541b3597ed8d8714a928c?url=https%3A%2F%2FRingtarih%40bitbucket.org%2Fcs3398-s22-borgs%2Fborgs.git

Task 3: Displayed weather UV index on the console/terminal. The code in this commit is very similar to the Weather API code but I pull the UV index value from the API being called and display only the UV index value on the terminal.
gitkraken://repolink/0abc55245efec5ccdfa9509557948d586d87394d/commit/8a6446eb2d8b758fee195b9104b7a49b5f0621f4?url=https%3A%2F%2FRingtarih%40bitbucket.org%2Fcs3398-s22-borgs%2Fborgs.git

Sprint 2:
Task 1: Researched on the the 32x32 LED Matrix shift registers and how they worked to display and image or text.
https://bitbucket.org/cs3398-s22-borgs/borgs/commits/4a81612abc1c36d52922a8f91a8d02931ea3f9e2

Task 2: Created code to display an image by scrolling it across the 32x32 LED matrix
https://bitbucket.org/cs3398-s22-borgs/borgs/commits/7e1d5f31fd4408c803e0a418a5cc52b948ebdfd5

Task 3: Created code to add functionality to the scroller code to display strings outside of the image that is in the repository
https://bitbucket.org/cs3398-s22-borgs/borgs/commits/df8188afe311697b02fc3c86c00b1d53b493a941

Sprint 3:
Jira Task BV-65: Refactored the cover art code for the raspberry pi.
https://bitbucket.org/cs3398-s22-borgs/borgs/commits/98500b71a7ee75237b32e444e8d6726f9ee15e86

Jira Task BV-58: Display cover art nicely on the screen. This was mainly refactoring the code to use certain pins to avoild static noise in the signals to the matrix.
https://bitbucket.org/cs3398-s22-borgs/borgs/commits/bcf8b0a57a91a0547c8e0bfad247793f0945b25b

Jira Task BV-66: Create sequence of display. This commit was geared towards making each code written by everyone seemlessly flow on the matrix.
https://bitbucket.org/cs3398-s22-borgs/borgs/commits/c84bc27a57e9742eaabda2cbf2aeba0b642aca30


## Usage
How does one go about using it?
Provide various use cases and code examples here.

`write-your-code-here`


## Project Status
Project is: _in progress_ 


## Next Steps
Nick - Get the word of the day program running on the screen.
Get form running on the screen with Anderson.
Familiarize myself with the hardware

Sprint 3:
Get routing program running on the led matrix
Refactor word of the day program
Get word of the day program displays well on the screen. (Make it look good)

Final Steps:
Get spotify program fully running on the display.

Uyen - Get the weather program running on the screen.
Get familiar with the LED screen.
Working with Ringtarih to get the screen fully working.

Sprint 3:
Refactor the time program and display on screen with API this time
Working with Ringtarih to get weather display well on screen without noise 

Final Step (Uyen): Next step is to develop the display on LED screen program that could display information in 2 rows running through the screen instead of only one row running display on screen. Develop the program display on screen to have a function that could display web form for task tracking program, a function that the user could input task of the day and display on screen. 


Anderson 
Sprint 1: Research libary imports for time, time zone and write a program that gets current time in current time zone and in different time zone.
Create a flask project that implements a form to take in form and saves it (task program)

Sprint 2: Get the time program running on the led matrix.
Get form running on the screen with Nick.
Get familiar with the hardware.

Sprint 3:  Get traffic program fully working on led matrix.
Refactor task program to console instead of web browswer.

Future Sprint: Get task program fully running on display and add a method on rasberry device to take inputs (to allow user to add task or reminders).
Research how microphone work with raspberry pi to get an idea of how to record sound. (Sound Alert idea not implemented but discussed).
Research API than can identify sounds (only need a few selected noise like door bell, fire alarm etc).


Ringtarih - Get the screen fully working without noise (static).
Work with Uyen to properly display the programs on the screen.

## Acknowledgements
Give credit here.
- This project was inspired by...
- This project was based on [this tutorial](https://www.example.com).
- Many thanks to...


## Contact
Created by [@flynerdpl](https://www.flynerd.pl/) - feel free to contact me!


<!-- Optional -->
<!-- ## License -->
<!-- This project is open source and available under the [... License](). -->

<!-- You don't have to include all sections - just the one's relevant to your project -->